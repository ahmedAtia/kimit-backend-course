<?php



function sendJsonResponse($data)
{
    header('Content-type: application/json');
    echo json_encode($data);
    exit();
}


function abort($code = 404, $data)
{
    http_response_code($code);
    echo json_encode($data);
    die();
}

function dump($toPrint)
{
    echo "<pre>";
    print_r($toPrint);
    echo "</pre>";
    echo "<hr>";
}

function dd($toPrint)
{
    dump($toPrint);
    die();
}


function redirect_to($location)
{
    header("Location: " . $location);
    exit;
}

function is_post_request()
{
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function is_get_request()
{
    return $_SERVER['REQUEST_METHOD'] == 'GET';
}

function public_path()
{
    return WWW_ROOT . "/";
}
