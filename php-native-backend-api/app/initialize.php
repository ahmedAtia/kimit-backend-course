<?php

//  FROM BROWER SIDE -> as a developer I need to take care of public resources ? static files
//  We need to find a dynamic way to locate our static resource by using server itself
// $_SERVER
define('WWW_ROOT', '/kimit-project/frontend');

//  FROM SERVER SIDE
define('APP_PATH', dirname(__FILE__));
define('PROJECT_PATH', dirname(APP_PATH));
define('MODELS_PATH', PROJECT_PATH . '/models');

require_once(APP_PATH . '/helpsFunction.php');

// require_once(APP_PATH . '/autoload.php');
require PROJECT_PATH  . '/vendor/autoload.php';


use \Myapp\Feeds\core\Request;

$request = new Request();
$db = new \Myapp\Feeds\core\DB();
$response = new \Myapp\Feeds\core\Response();
