<?php

namespace Myapp\Feeds\core;

class Response
{
    function json($data)
    {
        header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }
}
