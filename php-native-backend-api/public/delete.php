<?php

require_once('../app/initialize.php');

use \app\models\Post;

$post = new Post($db);

$to_delete_post_id = $request->get('post_id');

$post->delete($to_delete_post_id);


$data = [
    'deleted_post_id' => $to_delete_post_id,
    'message' => 'Post deleted successfully'
];




$response->json($data);


// json -> post_id deleted