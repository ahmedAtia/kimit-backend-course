
/*
This guide demonstrates the five fundamental steps
of database interaction using PHP.
oop vs functional vs procedural
procedural
functional
OOP programming
*/
//  object => class
// dd('die--->----------');
// 2. Perform database query
// SELECT * FROM social_db.posts;


// php native -> php functional -> php oop -> mvc => framework (nodejs, laravel, smfony, cakephp) json -> ajax

// javascript -> functional ( map, reduce, filter)(callback) -> ajax -> oop --> node.js (runtime environment) framework(react, vue, angluar, svile)