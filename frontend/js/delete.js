/**
    * @description      : 
    * @author           : 
    * @group            : 
    * @created          : 02/04/2023 - 16:09:41
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 02/04/2023
    * - Author          : 
    * - Modification    : 
**/

function fireAjaxDeleteRequestToServer(event, clickedDeleteButton) {

    dump(event);
    dump(clickedDeleteButton);

    const postId = clickedDeleteButton.getAttribute('our_custom_post_id');
    const deleteUrl = 'http://localhost/project_g2_posts/public/delete.php?post_id=' + postId;


    function success() {

        var data = JSON.parse(this.responseText);
        dump(data);
        let targetPostId = 'post-' + data.deleted_post_id;
        console.log(targetPostId);
        let post = document.getElementById(targetPostId);
        post.style.display = "none";
    }

    function error(err) {
        console.log('Error Occurred :', err);
    }

    var xhr = new XMLHttpRequest();
    xhr.onload = success;
    xhr.onerror = error;
    xhr.open('GET', deleteUrl, true);
    // xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
    xhr.send();
    xhr.onreadystatechange = function () {
        dump(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == 200) {
            dump(xhr.responseText);
            // document.getElementById("result").innerHTML = xhr.responseText;
        }
    }

}