<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function listPosts()
    {
        $allPost = \DB::select('
        SELECT 
			*,
            posts.id as post_id 
            FROM posts
            left join users
                on posts.creator_id = users.id
            ORDER BY posts.id DESC;
    ');
        // $allPost = Post::all();
        return response()->json($allPost);
    }


    public function editPost($id, Request $request)
    {
        $post = Post::find($id);
        $data = [];
        $data['title'] =  $request->get('title');
        $data['content'] =  $request->get('content');
        $data['post_image'] = $request->get('post_image');

        $post->update($data);

        return response()->json($post->refresh());
    }


    function deletePost(Request $request, $id)
    {
        Post::destroy($id);
        return response()->json([
            "post $id deleted sucessfully"
        ]);
    }
}
