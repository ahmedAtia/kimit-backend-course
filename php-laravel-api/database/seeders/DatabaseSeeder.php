<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Ahmed Attia',
        //     'email' => 'my@example.com',
        //     'password' => bcrypt('123456'),
        //     'is_active' => 1,
        //     'avatar_url' => 'https://lh3.googleusercontent.com/ogw/AAEL6shLj3lSzPZAJ-KlMymV07pMVmGIvLp4dYhihvRSLQ=s32-c-mo'
        // ]);

        \App\Models\Post::factory(100)->create();
    }
}
