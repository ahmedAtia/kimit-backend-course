<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// route -> controller -> model -> controller -> json / html mvc

// Route::get('/user/{id}', [UserController::class, 'show']);

Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', [UserController::class, 'details']);
    Route::get('/post/list', [PostController::class, 'listPosts']);
    Route::get('/post/edit/{id}', [PostController::class, 'editPost']);
    Route::get('/post/delete/{id}', [PostController::class, 'deletePost']);
});
